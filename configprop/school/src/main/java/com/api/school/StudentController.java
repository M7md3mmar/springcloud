package com.api.school;

import java.util.ArrayList;
import java.util.List;

import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.api.school.dtos.Student;


@RestController
@RequestMapping("/student")
public class StudentController {

	@GetMapping("/getStudents")
	public List<Student> getStudent()
	{
	List<Student> list=new ArrayList<>();
	list.add(new Student("mohamed","12345",15));
	list.add(new Student("fady","6789",16));
	
	return list;
	}
}
