package com.api.school;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.cloud.netflix.hystrix.EnableHystrix;

@SpringBootApplication
@EnableDiscoveryClient
@RefreshScope
@EnableHystrix
public class SchoolApplication {

	
	public static void main(String[] args) {
		SpringApplication.run(SchoolApplication.class, args);
	}
	

	
}
