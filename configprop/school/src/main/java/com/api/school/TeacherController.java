package com.api.school;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.api.school.dtos.Teacher;


@RestController
@RefreshScope
@RequestMapping("/teacher")
public class TeacherController {

	@Value("${welcome.message}")
	String welcomeText;

	@GetMapping("/getTeachers")
	public List<Teacher> getTeacher() {
		List<Teacher> list = new ArrayList<>();
		list.add(new Teacher("ali", 40, "math"));
		list.add(new Teacher("hany", 35, "english"));

		return list;
	}

	@RequestMapping(value = "/welcome")
	public String welcomeText() {
		return welcomeText;
	}

}
